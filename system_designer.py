# This script produces an antenna length vs power analisys and allows to choose one design selecting the antenna length

# %% imports
import numpy as np
from matplotlib import pyplot as plt
from tqdm import tqdm

from swath_optimization_functions import RangeOptimizationProblem
from geometryRadar import RadarGeometry
from farField import UniformAperture

# %% System parameters table
# center frequency 10GHz (default)
# antenna width
wa = 0.3
# incidence angle
theta = 30
# altitude
altitude = 500e3
# swath
swath = 25e3
# losses
l = 5  # dB
n_f = 5  # dB
L = 10 ** ((l + n_f) / 10)
# resolution Area
ares = 3  # m2
# wavelength
f = 10e9
c = 299792458.0
wavel = c / f
# probability of detection 0.5
# probability of false alarm 10^-12
# antenna temperature
Tant = 300  # k

# %% Swath optimization sweep
# geometry
radGeo = RadarGeometry()
#   altitude
radGeo.set_initial_position(0, 0, altitude)
#   rotation
radGeo.set_rotation(theta / 180 * np.pi, 0, 0)
#   speed
radGeo.set_speed(radGeo.orbital_speed())
# antenna
#   initial length
la = 3
antenna = UniformAperture(la, wa)
# problem creation
opti = RangeOptimizationProblem(radGeo, antenna, wavel)
#   swath setting
opti.swath = swath
# %% Sweep
antenna_length = np.linspace(1, 4, 64)
C_min = np.zeros_like(antenna_length)
for rr in tqdm(range(len(antenna_length))):
    # set antenna length
    opti.aperture.set_length(antenna_length[rr])
    # get minimum power over bandwidth
    opti.optimize()
    # get core snr
    C_min[rr] = np.average(opti.snr_core_edge)

# %% denormalization
NESZ_min = 10 ** ((18 / 5 - 8 / 5 * ares) / 10)
NESZ_min = 10 ** (-1/10)
PoverB = L * Tant / (NESZ_min * C_min)
B = opti.c_light * antenna_length / (4 * ares * np.sin(theta * np.pi / 180))
P = PoverB * B
# %% plotting
fig1, ax = plt.subplots(1)
plt.title(str("Ares = ") + str(ares))
ax2 = ax.twinx()
ax.plot(antenna_length, P, label='theta = ' + str(theta))
ax2.plot(antenna_length, B / 1e6, '--', label='theta = ' + str(theta))
ax.legend()
ax.set_xlabel('antenna length [m]')
ax.set_ylabel('P_min [W]  _____')
ax2.set_ylabel('B [MHz] - - - -')
ax.grid()
ax.grid()

#%% Final design
# %% Sweep
antenna_length = np.array([2.8])
C_min = np.zeros_like(antenna_length)
for rr in tqdm(range(len(antenna_length))):
    # set antenna length
    opti.aperture.set_length(antenna_length[rr])
    # get minimum power over bandwidth
    opti.optimize()
    # get core snr
    C_min[rr] = np.average(opti.snr_core_edge)

# %% denormalization
NESZ_min = 10 ** ((18 / 5 - 8 / 5 * ares) / 10)
NESZ_min = 10 ** (-1/10)
PoverB = L * Tant / (NESZ_min * C_min)
B = opti.c_light * antenna_length / (4 * ares * np.sin(theta * np.pi / 180))
P = PoverB * B
print(" power = ", P)
print(" bandwidth = ", B)