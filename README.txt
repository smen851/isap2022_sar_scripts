9 June 2022



This project contains the scripts to produce the plot in the article for ISAP2022
most of the function and classes are adapted from the dynamic-range project


- the flat earth to spherical earth transformations are explained in the notebook flat_earth_removal.ipynb

- the figure in the paper is produced with "swath_trade_off.py" and modified in inkscape

- the design example in the paper is produced using the "system_designer.py" script