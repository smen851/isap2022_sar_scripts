# the use of these functions is demonstrated in teh flat_earth_removal.ipynb notebook
import numpy as np
from numpy import sin, cos, tan, arctan
from scipy.optimize import root_scalar


def groundequ(rgf, rgs, h, re=6371e3):
    """
    find the zero of this function
    :param rgf: flat earth ground range
    :param rgs: spherical earth ground range
    :param h: orbital height
    :param re: optional. earth radius, default 6371 km
    :return: the equation to be optimized
    """
    f = - arctan(rgf / h) + \
        arctan(re * sin(rgs / re) /
               (h - re * cos(rgs / re) + re)) + \
        rgs / re
    return f


def theta(rgf, h):
    return arctan(rgf / h)


@np.vectorize
def rgf2rgs_incidence(rgf, h):
    """
    converts ground range on flat earth to ground range on spherical earth preserving the incidence angle
    :param rgf: flat earth ground range
    :param h: orbital height
    :return: the spherical earth ground range
    """
    rgs = root_scalar(lambda x: groundequ(rgf, x, h), method='secant',
                      x0=rgf - 100,
                      x1=rgf + 100)
    print(rgs.root)
    return float(rgs.root)


@np.vectorize
def rgs2rgf_incidence(rgs, h, re=6371e3):
    """
    converts ground range on spherical earth to ground range on flat earth preserving the incidence angle
    :param re: optional earth radius, default 6371km
    :param rgs: spherical earth ground range
    :param h: orbital height
    :return: the spherical earth ground range
    """
    rgf = h * tan(arctan(re * sin(rgs / re) / (h - re * cos(rgs / re) + re)) + rgs / re)
    #       [H*tan(atan(R_E*sin(R_{GSE}/R_E)/(H - R_E*cos(R_{GSE}/R_E) + R_E)) + R_{GSE}/R_E)]
    return rgf


def r1(rgs, rgf, h, re=6371e3):
    Theta = theta(rgf, h)
    r1 = (h + re * (1 - cos(rgs / re))) / cos(Theta - rgs / re)
    return r1


def r(rgf, h):
    return np.sqrt(h ** 2 + rgf ** 2)


def true_range_error_fractional(rgf, rgs, h, re=6371e3):
    """
    finds the ratio between the spherical earth true range and the flat earth range
    :param rgf: flat earth ground range
    :param rgs: spherical earth ground range
    :param h: orbital height
    :param re: optional. earth radius, default 6371 km
    :return: R' / R
    """
    Theta = theta(rgf, h)
    err = (h + re * (1 - cos(rgs / re))) * cos(Theta) / (h * cos(Theta - rgs / re))
    return err
