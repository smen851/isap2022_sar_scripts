#%%
import matplotlib.pyplot as plt
from tqdm import tqdm
from matplotlib import cm

from swath_optimization_functions import RangeOptimizationProblem, core_SNR, theor_core_SNR
from geometryRadar import RadarGeometry
from farField import UniformAperture
from flat_earth_removal_functions import rgf2rgs_incidence
import numpy as np

# %% problem model
# antenna
la = 2
wa = .3
f = 10e9
antenna = UniformAperture(la, wa, frequency=f)
# wavelength
c = 299792458.0
wavel = c / f
# create a radar geometry
radGeo = RadarGeometry()
#   looking angle deg
side_looking_angle = 30  # degrees #todo consider here the spherical earth incidence angle
radGeo.set_rotation(side_looking_angle / 180 * np.pi, 0, 0)
#   altitude
altitude = 500e3  # m
radGeo.set_initial_position(0, 0, altitude)
#   speed
radGeo.set_speed(radGeo.orbital_speed())
# problem creation
opti = RangeOptimizationProblem(radGeo, antenna, wavel)
# %% boundaries
# nominal swath
rmin = altitude * np.tan(side_looking_angle / 180 * np.pi - wavel / wa / 2)
rmax = altitude * np.tan(side_looking_angle / 180 * np.pi + wavel / wa / 2)
ground_range_axis = np.linspace(rmin, rmax, 200)
ground_range_axis_sphere = rgf2rgs_incidence(ground_range_axis, altitude)
snr_core = core_SNR(radGeo, antenna, -ground_range_axis, wavel)

#%% plot setup
import matplotlib.font_manager as font_manager
fig, [ax,ax1] = plt.subplots(nrows=1, ncols=2, sharey =True, gridspec_kw={'width_ratios': [11, 5]})
axis_font = {'fontname':'Times New Roman', 'size':'8'}
ax.plot(ground_range_axis_sphere/1000, 10*np.log10(snr_core),'k',label='C$_{true}$')
ax.set_xlabel('(a) Ground range [km]', **axis_font)
ax.set_ylabel('C dB', **axis_font)

#%% ideal snr equation
core_ideal = theor_core_SNR(radGeo, antenna, -ground_range_axis, wavel)
ax.plot(ground_range_axis_sphere/1000, 10*np.log10(core_ideal), '--g',label='C$_{ideal}$')
#ax.plot(ground_range_axis_sphere/1000, 10*np.log10(core_ideal) - 6, '--b')
# %% coarse swath sweep
swath = np.linspace(10, 50, 5)*1e3
r_near = np.zeros_like(swath)
r_far = np.zeros_like(swath)
swath_center = np.zeros_like(swath)
core_c = np.zeros_like(swath)
for ii in tqdm(range(len(swath))):
    opti.swath = swath[ii]
    r_near[ii], r_far[ii], foo = opti.optimize(spherical=True)
    swath_center[ii] = (r_far[ii] + r_near[ii]) / 2
    r = np.array([r_near[ii], r_far[ii]])
    core = core_SNR(radGeo, antenna, r, wavel)
    core_c[ii] = core[0]
    ax.plot(rgf2rgs_incidence(-r, altitude) / 1000, 10 * np.log10(core), label=str(int(swath[ii]/1000))+str(' km'), color=cm.get_cmap('tab10').colors[ii])
    #ax.plot(rgf2rgs_incidence(-swath_center[ii], altitude) / 1000, 10 * np.log10(core[0]), 'x', color=cm.get_cmap('tab10').colors[ii])
#ax.legend()

#%% fine swath sweep
swath_f = np.linspace(5, 70, 80)*1e3
r_near_f = np.zeros_like(swath_f)
r_far_f = np.zeros_like(swath_f)
swath_center_f = np.zeros_like(swath_f)
core = np.zeros_like(swath_f)
for ii in tqdm(range(len(swath_f))):
    opti.swath = swath_f[ii]
    r_near_f[ii], r_far_f[ii], foo = opti.optimize(spherical=True)
    swath_center_f[ii] = (r_far_f[ii] + r_near_f[ii]) / 2
    r = np.array(r_far_f[ii])
    core[ii] = core_SNR(radGeo, antenna, r, wavel)
ax1.plot(swath_f/1000, 10*np.log10(core),'k', label='C$_{min}$')
#%% points
for ii in tqdm(range(len(swath))):
    (markers, stemlines, baseline) = ax1.stem(swath[ii] / 1000, 10 * np.log10(core_c[ii]),label=str(int(swath[ii]/1000))+str(' km'))
    plt.setp(stemlines, color=cm.get_cmap('tab10').colors[ii])
    plt.setp(markers, mfc=cm.get_cmap('tab10').colors[ii], mec=cm.get_cmap('tab10').colors[ii])

ax.grid()
ax1.grid()
#%%
ax1.set_xlabel('(b) Swath width [km]', **axis_font)

ax.set_xlim(ground_range_axis_sphere[0]/1000, ground_range_axis_sphere[-1]/1000)
ax.set_ylim(90, 99)
ax1.set_xlim(0, 60)
ax1.set_xticks(swath/1000,np.array(swath/1000).astype('int'), rotation='vertical')
fig.set_size_inches(3.47, 2)
plt.subplots_adjust(left=0.11, bottom=.19, right=.98, top=1, wspace=.08)
plt.show()

#%%
import matplotlib.pyplot as plt
import matplotlib.font_manager as font_manager
# Set the tick labels font
for label in (ax.get_xticklabels() + ax.get_yticklabels()):
    label.set_fontname('Times New Roman')
    label.set_fontsize(8)
for label in (ax1.get_xticklabels() + ax1.get_yticklabels()):
    label.set_fontname('Times New Roman')
    label.set_fontsize(8)
#%% set the legend font
font_prop = font_manager.FontProperties(fname="C:/Windows/Fonts/times.ttf", size=8)
ax.legend(loc='lower right', prop=font_prop, numpoints=1)

plt.show()